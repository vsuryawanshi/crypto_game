import React from 'react';
import { render } from 'react-dom';
import '../styles/main.scss';

import AppContainer from "./containers/App"

render(
    <AppContainer />, document.getElementById('appcontainer')
);

window.onload = function(){
    var loaderElem = document.getElementById("loader-wrap");
    loaderElem.parentNode.removeChild(loaderElem);
}