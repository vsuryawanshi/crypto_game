import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';

import Async from 'react-code-splitting'

const landing = () => <Async load={import(/* webpackChunkName: "landing" */'./Landing')} />
const userProfile = () => <Async load={import(/* webpackChunkName: "userprofile" */'./UserProfile')} />



export default class AppContainer extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact={true} component={landing} />
                    <Route path='/user' exact={true} component={userProfile} />
                    <Route path='*' component={() => <AsyncComponent moduleProvider={landing} />} />
                </Switch>
            </Router>
        );
    }
}