import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class LandingPage extends Component {
    render() {
        return (
            <div className="landing-wrapper">
                This is the landing page
                <Link to="/user">
                    <button>User Profile</button>
                </Link>
                
            </div>
        );
    }
}